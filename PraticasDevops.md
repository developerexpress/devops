
# Praticas

A integração contínua é uma prática de desenvolvimento de software em que os desenvolvedores, com frequência, juntam suas alterações de código em um repositório central. Depois disso, criações e testes são executados. Os principais objetivos da integração contínua são encontrar e investigar erros mais rapidamente, melhorar a qualidade do software e reduzir o tempo necessário para validar e lançar novas atualizações de software.

**Entrega contínua**

A entrega contínua é uma prática de desenvolvimento de software em que alterações de código são criadas, testadas e preparadas automaticamente para liberação para produção.

**Microsserviços**

A arquitetura de microsserviços é uma abordagem de projeto para a criação de um aplicativo único como um conjunto de pequenos serviços. Cada serviço é executado em seu próprio processo e se comunica com outros serviços por meio de uma interface bem definida usando um mecanismo leve, geralmente uma interface de programação de aplicativo (API) baseada em HTTP.

**Infraestrutura como código**

A infraestrutura como código é uma prática em que a infraestrutura é provisionada e gerenciada usando técnicas de desenvolvimento de código e software, como controle de versão e integração contínua. O modelo controlado por API da nuvem permite que desenvolvedores e administradores de sistema interajam com a infraestrutura de modo programático e em escala, em vez de precisarem instalar e configurar manualmente os recursos

**Monitoramento e registro em log**

As empresas monitoram métricas e logs para ver como a performance do aplicativo e da infraestrutura afeta a experiência do usuário final do seu produto. Como os serviços devem estar disponíveis 24/7 e a frequência de atualização do aplicativo e da infraestrutura aumenta, o monitoramento ativo torna-se cada vez mais importante. A criação de alertas ou a execução de análise em tempo real desses dados também ajuda as empresas a monitorar de modo mais proativo seus serviços.

**Comunicação e colaboração**

O aumento da comunicação e da colaboração em uma empresa é um dos principais aspectos culturais do DevOps. O uso das ferramentas de DevOps e da automação do processo de entrega de software estabelece a colaboração ao unir fisicamente os fluxos de trabalho e as responsabilidades de desenvolvimento e operações. Baseando-se nisso, essas equipes definem normas culturais sólidas com relação ao compartilhamento de informações, além de facilitar a comunicação por meio do uso de aplicativos de chat, sistemas de acompanhamento de problemas ou projetos, e wikis. Isso ajuda a agilizar a comunicação entre desenvolvedores, operações e até mesmo outras equipes, como marketing ou vendas, permitindo que todas as partes da empresa se alinhem mais estreitamente às metas e aos projetos.
