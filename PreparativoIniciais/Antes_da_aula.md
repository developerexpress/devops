# Inicio do começo

Bom, sempre precisamos começar de alguma forma, correto? 

Antes do curso, temos algumas configurações que precisam ser realizadas para já dar aquela agilizada no serviço. 

No modulo sobre DevOps, iremos utilizar duas ferramentas básicas o tão famoso <i>Docker</i> e ~~um editor de texto metido a besta~~ IDE, que é onde iremos escrever os códigos. 

O processo de instalação e configuração é bem "simples" e pode ser seguindo a instalação padrão (next, next, install)

Caso tenha alguma duvida, pode enviar no telegram que já iremos auxiliar nas configurações. 

## Materiais necessários

1. VS Code - > instalação padrão https://code.visualstudio.com/download
2. Docker - > https://docs.docker.com/docker-for-windows/install/