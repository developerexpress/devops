# Kubernetes 

## Han? 

"O K8s é um projeto de código aberto que tem como objetivo orquestrar containers e automatizar a implantação de aplicações."


![K8S](/images/k8s.png "K8S")

>Kubernetes (κυβερνήτης, a palavra grega para "timoneiro" ou "piloto") foi fundado por uma galera de engenharia  da Google, e foi anunciado pelo Google em meados de 2014.



## Por que precisamos dele?

Os contêineres são uma boa maneira de agrupar e executar suas aplicações.
Em um ambiente de produção, você precisa gerenciar os contêineres que executam as aplicações e garantir que não haja tempo de inatividade. 
Por exemplo, **se um contêiner cair, outro contêiner precisa ser iniciado**. Não seria mais fácil se esse comportamento fosse controlado por um sistema?.

- **Descoberta de serviço e balanceamento de carga**: pode expor um contêiner usando o nome DNS ou seu próprio endereço IP.

- **Orquestração de armazenamento**: permite que você monte automaticamente um sistema de armazenamento de sua escolha, como armazenamentos locais, provedores de nuvem pública.

- **Lançamentos e reversões automatizadas**: você pode automatizar o Kubernetes para criar novos contêineres para sua implantação, remover os contêineres existentes e adotar todos os seus recursos para o novo contêiner.

- **Empacotamento binário automático**: você informa ao Kubernetes de quanta CPU e memória (RAM) cada contêiner precisa.

- **Autocorreção**:  reinicia os contêineres que falham, substitui os contêineres, elimina os contêineres que não respondem à verificação de integridade definida pelo usuário.

- **Gerenciamento de configuração e de segredos**: 0 Kubernetes permite armazenar e gerenciar informações confidenciais, como senhas, tokens OAuth e chaves SSH




## Tudo o que tem

![componentes](/images/components-of-kubernetes.svg "componentes")




### Gerenciamento 
 - **kube-apiserver**: O servidor de API é um componente da Camada de gerenciamento do Kubernetes que expõe a API do Kubernetes.

 - **kube-scheduler**: Componente da camada de gerenciamento que observa os pods recém-criados sem nenhum nó atribuído, e seleciona um nó para executá-los.

 - **kube-controller-manager**: Executa os processos de controladores, como o *Controlador de nó*, *Controlador de Job*, *Controlador de endpoints*, *Controladores de conta de serviço e de token*

![master_node](/images/master_node.png "kube_arch")


### Componentes de nós

- **kubelet**: Um agente que é executado em cada node no cluster. Ele garante que os contêineres estejam sendo executados em um Pod.

- **kube-proxy**: roxy de rede executado em cada nó no seu cluster

- **Container runtime**: O agente de execução (runtime) de contêiner é o software responsável por executar os contêineres.


![node](/images/node.png "node")

### Pods 
Pods são a unidade atômica na plataforma Kubernetes. Quando criamos um Deployment no Kubernetes, esse Deployment cria Pods com contêineres dentro dele 

**Menor parte do Kubernetes**

- Um ambiente para executar contêineres
- Tem pilha de rede, namespaces do kernel e um ou mais contêiner em execução
- Pod pode ter vários contêineres
- É a unidade que escala no k8s

![pods](/images/pods.png "pods")


### Services
Os pods vêm e vão com diferentes IPs. Para distribuir carga e atuar como uma única fonte de interação para todos os pods de um aplicativo, o serviço desempenha o papel.

- Tem um único IP e DNS
- Criado com um arquivo JSON de manifesto
- Todos os novos pods são adicionados / registrados ao serviço
- Qual pod deve ser atribuído a quais serviços são decididos pelos rótulos
- Serviço e pods têm rótulos com base em qual serviço identifica seus pods
- Envia tráfego apenas para pods saudáveis
- Serviço pode apontar coisas fora do cluster

![service](/images/service.png "service")



### Como os componentes trabalham


- O kubectl grava no servidor da API
- O servidor de API valida a solicitação e a persiste para o armazenamento de cluster (etcd)
- Armazenamento de Cluster (etcd) notifica o servidor de API de volta
- O servidor de API chama o agendador
- O agendador decide onde executar o pod e retorna isso ao servidor de API
- API Server persiste para o etcd
- O etcd notifica o servidor da API.
- O servidor de API chama o Kubelet no nó correspondente
- Kubelet fala com o daemon do Docker usando a API sobre o soquete do Docker para criar o contêiner
- Kubelet atualiza o status do pod para o servidor da API
- API Server persiste o novo estado no etcd


![kube_arch](/images/kube_arch.png "kube_arch")


# HANDS ON

Docs úteis

>https://docs.microsoft.com/pt-br/azure/aks/kubernetes-walkthrough

>https://docs.microsoft.com/pt-br/azure/aks/

>https://docs.microsoft.com/pt-br/azure/aks/intro-kubernetes


## Configurar 

### Criar um grupo de recursos

>az group create --name CursoDevAKS --location brazilsoutheast

### Criar um cluster AKS 

>az aks create --resource-group CursoDevAKS --name CursoAKSCluster --node-count 1  --generate-ssh-keys


### Acessar o cluster

Instala o kubectl localmente 
>az aks install-cli 

Configura o kubectl para acesso maneira

>az aks get-credentials --resource-group myResourceGroup --name myAKSCluster


### Comandos do cluster

Lista todos os nós
>kubectl get nodes


## Rodar a aplicação de exemplo

Um arquivo de manifesto do Kubernetes define o estado desejado de um cluster, por exemplo, as imagens de contêiner a serem executadas.

Nesse exemplo, vamos usar o "Azure Vote" para teste


>kubectl apply -f azure-vote.yaml


## Testando? 

Serviço de Kubernetes expõe o front-end do aplicativo à Internet, isso pode demorar um pouco 

Para acompanhar o processos
>kubectl get service azure-vote-front --watch



## Deleta tudo


az group delete --name CursoDevAKS --yes --no-wait



